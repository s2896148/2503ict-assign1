drop table if exists post;
drop table if exists comment;

create table post (
  id integer primary key autoincrement,
  PostTime text not null,
  PostUser varchar(40) not null,
  Title varchar(200) not null,
  Post varchar(4000) not null
);

create table comment (
  id integer primary key autoincrement,
  postId integer not null,
  CommentTime text not null,
  CommentUser varchar(40) not null,
  Comment varchar(4000) not null
);
