<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bookface</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="/public/css/style.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{secure_url('/')}}">Bookface</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="./">Photos</a></li>
              <li><a href="documentation">Documentation</a></li>
              <li><a href="./">Login</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main body -->
        <div class = 'row'>
            <div class= 'col-sm-4'>
                @section ('post')
                <form method="post" action="{{{ url('add_post_action') }}}">
                    Title: <br>
                    <input type= 'text' id='title' name= 'title' maxlength="200" value=""> <br>
                    Username: <br>
                    <input type= 'text' id='username' name= 'username' maxlength="40"value=""> <br>
                    Message: <br>
                    <textarea rows = '4' cols='25' name='message' maxlength="4000" placeholder="Enter Your Message"></textarea> <br>
                    <input type="submit" value="Post">
                </form>
                @show
            </div>
            <div class= 'col-sm-8'>
            @yield ('content')

            </div>
        </div>

    </div> <!-- /container -->
    
  </body>
</html>
