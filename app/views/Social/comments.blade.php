@extends('Layouts.master')

@section('post')
@foreach($result as $postItem)
       <div class='postcommentsborder'>
           <form method="post" action="{{{ url('add_comment_action') }}}">
                    <strong>Add your Comment</strong><br>
                    Username: <br>
                    <input type= 'text' id='username' name= 'username' maxlength="40"value=""> <br>
                    Message: <br>
                    <textarea rows = '2' cols='25' name='message' maxlength="4000" placeholder="Enter Your comment"></textarea><br>
                    <input type= 'text' id='id' name= 'id' style="display:none;"  value="{{{ $postItem->id }}}">
                    <input type="submit" value="Comment">
                    <button type="button" onclick="window.location='{{ url("home") }}'">Back</button>
            </form>
        </div>
@endforeach
@stop
@section('content')
@foreach($result as $postItem)
       <div class ='post'>
           <img class='photo' src= '/public/logo.jpg' alt='photo'>
            <br><strong>Time of Post: </strong>{{{$postItem->PostTime}}}
            <br><strong>User: </strong>{{{$postItem->PostUser}}}
            <br><strong>Title: </strong>{{{$postItem->Title}}}<br>
            {{{$postItem->Post}}}<br>
       </div>
       <p><strong>Comments:</strong></p>
@endforeach
@if (count($comments) == 0)
   
        <br><p>No Comments Found</p>
@else
    @foreach($comments as $commentItem)
             <div class ='post'>
               <img class='photo' src= '/public/logo.jpg' alt='photo'>
                <br><strong>Time of Comment: </strong>{{{$commentItem->CommentTime}}}
                <br><strong>User: </strong> {{{$commentItem->CommentUser}}}
               <p class = 'comment'> {{{$commentItem->Comment}}}</p>
                <div class='footer'>
                  <a href='{{{url("deleteComment/$commentItem->id")}}}'>Delete</a>
                </div>
           </div>
       
    @endforeach
@endif
@stop