@extends ('Layouts.master')


@section ('post')
<Strong>Create Database Code:</Strong><br>
<br>drop table if exists post;<br>
drop table if exists comment;<br>

<br>create table post (<br>
  id integer primary key autoincrement,<br>
  PostTime text not null,<br>
  PostUser varchar(40) not null,<br>
  Title varchar(200) not null,<br>
  Post varchar(4000) not null<br>
);<br>

<br>create table comment (<br>
  id integer primary key autoincrement,<br>
  postId integer not null,<br>
  CommentTime text not null,<br>
  CommentUser varchar(40) not null,<br>
  Comment varchar(4000) not null<br>
);

@stop
@section('content')
    <Strong>Post Mortem:</Strong><br>
   <br>I am with my final results, as I was able to produce what this task required.<br>
   One of the only areas I had difficulty with, was when placing the documentation page in the public/docs.<br>
   Though to fix this I choose to leave the file within the social tab. <br>
   Apart from this they were no other significant changes or extra implementations done for this project.<br>
   <br> <Strong>ER Diagram:</Strong>
    <img class='er' src= '/public/docs/ER.png' alt='photo'><br>
    <Strong>Site Map:</Strong><br>
    <img class='er' src= '/public/docs/SiteMap.png' alt='photo'>
@stop