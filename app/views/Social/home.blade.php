@extends ('Layouts.master')


@section ('post')

@section('content')
@if (count($results) == 0)
    <p>No Posts Found</p>
@else
   @foreach($results as $postItem)
       <div class ='post'>
           <img class='photo' src= 'logo.jpg' alt='photo'>
            <br><strong>Time of Post: </strong>{{{$postItem->PostTime}}}
            <br><strong>User: </strong>{{{$postItem->PostUser}}}
            <br><strong>Title: </strong>{{{$postItem->Title}}}<br>
            {{{$postItem->Post}}}<br>
            <br><div class='footer'>
                <a href='{{{url("edit/$postItem->id")}}}'>Edit |</a>
                <a href='{{{url("comments/$postItem->id")}}}'>View Comments ({{{$postItem->numComments}}}) | </a>
                <a href='{{{url("delete/$postItem->id")}}}'>Delete</a>
            </div>
       </div>
   @endforeach 
@endif
@stop