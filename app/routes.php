<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
    $result=get_posts();
    return View::make('Social.home')->withResults($result);
    //$results="";
});

Route::get ('documentation', function()
{
    return View::make('Social.documentation');
});

Route::get ('home', function()
{
        $result=get_posts();
    return View::make('Social.home')->withResults($result);
});

Route::get ('comments', function($postId)
{
    $comments=get_comments($postId);
     $result=get_post($postId);
     return View::make('Social.comments')->withResult($result)->withComments($comments);
});

Route::post('add_post_action', function()
{
  $title = Input::get('title');
  $username = Input::get('username');
  $message = Input::get('message');
  if ($title!="")
  {
      $id=add_post($title, $username, $message);
      if($id){
           $result=get_posts();
          return View::make('Social.home')->withResults($result);
      }else{
          die("Error adding post");
      }
  }else{
        $result=get_posts();
        return View::make('Social.home')->withResults($result);
  }
});

Route::post('edit_post_action', function()
{
    $id = Input::get('id');
    $title = Input::get('title');
    $username = Input::get('username');
    $message = Input::get('message');
    $update=update_post($id, $title, $username, $message);
    $result=get_posts();
    return View::make('Social.home')->withResults($result);
});

Route::post('delete_post_action', function()
{
    $id = Input::get('id');
    $update=delete_post($id);
     $update=delete_postcomments($id);
    $result=get_posts();
    return View::make('Social.home')->withResults($result);
});

Route::post('delete_comment_action', function()
{
    $id = Input::get('id');
    $postId = Input::get('PostId');
    $update=delete_comments($id);
    $comments=get_comments($postId);
    $result=get_post($postId);
    return View::make('Social.comments')->withResult($result)->withComments($comments);
});

Route::post('add_comment_action', function()
{
  $postId = Input::get('id');
  $username = Input::get('username');
  $message = Input::get('message');
  if ($message!="")
  {
      $id=add_comments($postId, $username, $message);
      if($id){
        $comments=get_comments($postId);
        $result=get_post($postId);
         return View::make('Social.comments')->withResult($result)->withComments($comments);
      }else{
          die("Error adding comment");
      }
  }else{
     $comments=get_comments($postId);
     $result=get_post($postId);
     return View::make('Social.comments')->withResult($result)->withComments($comments);
  }
});

function get_posts()
{
   //$sql = "select * from post order by PostTime desc";
    $sql = "select count(comment.id) as numComments, post.id as id, PostTime, PostUser, Title, Post from comment, post where comment.postId = post.id group by post.id, PostTime, PostUser, Title, Post union select 0 as numComments, post.id as id, PostTime, PostUser, Title, Post from post where not exists(select* from comment where postId = post.id) order by 3 desc";
    $result = DB::select($sql);
    return $result;
    
}

function get_post($id)
{
    $sql = "select * from post where id = ?";
    $result = DB::select($sql, array($id));
    return $result;
    
}

function delete_post($id)
{
    $sql = "delete from post where id = ?";
    $result = DB::delete($sql, array($id));
    return $result;
    
}

function delete_comments($id)
{
    $sql = "delete from comment where id = ?";
    $result = DB::delete($sql, array($id));
    return $result;
    
}

function delete_postcomments($id)
{
    $sql = "delete from comment where postId = ?";
    $result = DB::delete($sql, array($id));
    return $result;
    
}

function get_comments($postId)
{
    $sql = "select * from comment where postId=? order by CommentTime desc";
    $result = DB::select($sql, array($postId));
    return $result;
    
}
function get_comment($id)
{
    $sql = "select * from comment where id=?";
    $result = DB::select($sql, array($id));
    return $result;
    
}
function add_comments($PostId, $username, $message)
{
 $sql = "insert into comment (postId, CommentUser, Comment, CommentTime) values (?, ?, ?, ?)";
 $CommentTime=date("Y-m-d H:i:s");
 DB::insert($sql, array($PostId, $username, $message, $CommentTime));
 return true;

} 

function add_post($title, $username, $message)
{
 $sql = "insert into post (Title, PostUser, Post, PostTime) values (?, ?, ?, ?)";
 $PostTime=date("Y-m-d H:i:s");
 DB::insert($sql, array($title, $username, $message, $PostTime));
 return true;

} 
function update_post($id, $title, $username, $message)
{
 $sql = "update post set Title=?, PostUser=?, Post=? where id=?";
 DB::update($sql, array($title, $username, $message, $id));
 return true;

} 
Route::get ('comments/{id}', function($id)
{
    $comments=get_comments($id);
    $result=get_post($id);
    return View::make('Social.comments')->withResult($result)->withComments($comments);
});
Route::get ('edit/{id}', function($id)
{
    $result=get_post($id);
    return View::make('Social.edit')->withResult($result);
});



Route::get ('delete/{id}', function($id)
{
    $result=get_post($id);
    return View::make('Social.delete')->withResult($result);
});

Route::get ('deleteComment/{id}', function($id)
{
    $result=get_comment($id);
    return View::make('Social.deleteComment')->withResult($result);
});
